const ethers = require('ethers');
const AbiCoder = require("ethers").utils.AbiCoder;
const abiCoder = new AbiCoder();
const abi = require('./executor.json');


const iFace = new ethers.utils.Interface(abi);

/*
* (SwapStep[], bytes, bytes4, uint256, uint256, uint256)
*
*
* struct SwapStep {
        address fromToken;
        address toToken;
        address pair;
        uint256 fee;
        uint256 protocol;
    }

    struct SwapPath {
        SwapStep[] steps;
        uint256 amountIn;
    }

    struct SwapInfo {
        address fromToken;
        address toToken;
        uint256 amountIn;
        uint256 amountOut;
        bytes4 selectorFunction;
    }

    struct Authenticator {
        bytes signature;
        uint256 deadline;
    }

        SwapPath[] memory _paths,
        SwapInfo memory _info,
        Authenticator memory _authenticator
        *
        bytes32 _hash = keccak256(abi.encode(address(this), _path, _deadline)).toEthSignedMessageHash();

* */
const sign = async (path, info, authen) => {

    // console.log("get signhash", iFace.getSighash('swapExactInput'));

    //encode data
    const data = abiCoder.encode(
        [
            "tuple(address,address,address,uint256,uint256,uint256,bool)[]",
            "tuple(address,address,uint256,uint256,bytes4)",
            "tuple(bytes,uint256)",
           ],
        [path, info, authen]
    );

    const dataArrayify = ethers.utils.arrayify(data);


    console.log("signature", data);
    // console.log("signature", data);
    // console.log("selector", iFace.getSighash('swapExactOutput'));
}
(async () => {
    const steps = [["0xd9145CCE52D386f254917e481eB44e9943F39138","0xd9145CCE52D386f254917e481eB44e9943F39138","0xd9145CCE52D386f254917e481eB44e9943F39138",30,0, 1000000000, false]];
    // const amountIn = 1000000000000
    // const path = [[steps, amountIn]];
    const swapInfo = ["0xd9145CCE52D386f254917e481eB44e9943F39138","0xd9145CCE52D386f254917e481eB44e9943F39138",1000000000000, 10000000000, iFace.getSighash('swapExactInput')];
    const signature = "0xfe847e72351665a044aa31ae837f856eafdc7978f0123a255a447f9f7daebccd2718f6eea1094d86e5c88cdc01504ea3a9b3004eeed6aec2a8a436accafddc941c";
    const deadline = 123345345345;
    const authenticator = [signature, deadline];
    await sign(
        steps,
        swapInfo,
        authenticator
    );

})().catch(err => {
    console.log(err);
})