const ethers = require('ethers');
const {Wallet} = require("ethers");
const AbiCoder = require("ethers").utils.AbiCoder;
const abiCoder = new AbiCoder();
const sha3 = require('js-sha3');


/*

 struct SwapStep {
        address fromToken;
        address toToken;
        address pair;
        uint256 fee;
        uint256 protocol;
        uint256 amountIn;
        bool isFinished;
    }

* */

const sign = async (privateKey, contract, steps, deadline) => {
    const wallet = new Wallet(privateKey);

    //encode data
    const data = abiCoder.encode(
        ["address","tuple(address,address,address,uint256,uint256,uint256,bool)[]","uint256"],
        [contract, steps, deadline]
    );

    const dataArrayify = ethers.utils.arrayify(data);
    const messageHash = '0x'+sha3.keccak_256(dataArrayify);

    // STEP 2: 32 bytes of data in Uint8Array
    let messageHashBinary = ethers.utils.arrayify(messageHash);

    // STEP 3: To sign the 32 bytes of data, make sure you pass in the data
    let signature = await wallet.signMessage(messageHashBinary);

    console.log("signature", signature);
}
(async () => {
    const steps = [["0xd9145CCE52D386f254917e481eB44e9943F39138","0xd9145CCE52D386f254917e481eB44e9943F39138","0xd9145CCE52D386f254917e481eB44e9943F39138",30,0, 1000000000, false]];
    const amountIn = 1000000000000
    const path = [[steps, amountIn]];
    await sign(
        "0x4d3018ad3043374d69c32b9b8cd7b9286adf7a940ef8dac5108f4d5500f85b4f",
        "0xd9145CCE52D386f254917e481eB44e9943F39138",
        steps,
        123123123
    );

})().catch(err => {
    console.log(err);
})