const ethers = require('ethers');
const {Wallet} = require("ethers");

const sign = async (userAddress, contractAddress, projectId, _slot, privateKey, tuple) => {
    const wallet = new Wallet(privateKey);

    // let messageHash = ethers.utils.solidityKeccak256(
    //     ["uint","address","address", "uint"],
    //     [projectId, userAddress, contractAddress, _slot]
    // );

    let messageHash = ethers.utils.solidityKeccak256(
        ["tuple(address,uint)[]"],
        [tuple]
    );

// // STEP 2: 32 bytes of data in Uint8Array
    let messageHashBinary = ethers.utils.arrayify(messageHash);


// STEP 3: To sign the 32 bytes of data, make sure you pass in the data
    let signature = await wallet.signMessage(messageHashBinary);

    console.log("signature", signature);
}

(async () => {
    await sign("0xc982c05870893A0BafD0B27Bc8AE4103df9fF357",
        "0xfa862F61D13172A7e44b1CAe273489FA48E6e6bC",
        127,
        4,"0x4d3018ad3043374d69c32b9b8cd7b9286adf7a940ef8dac5108f4d5500f85b4f",
    [["0xc982c05870893A0BafD0B27Bc8AE4103df9fF357",1]]);
})().catch(err => {
    console.log(err);
})